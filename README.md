# uvod.mapathon.cz
Web page with the initial presentation showed at mapathons in CZ & SK.

# Contribute
## Code
For quick orientation see the [changelog][].

Please, think about [The seven rules of a great Git commit message][] when
making commit. The project use [OneFlow][] branching model with the `master`
branch as the branch where the development happens.

## License
This project is developed under [GNU GPLv3 license][].

[changelog]: ./CHANGELOG.md
[The seven rules of a great Git commit message]: https://chris.beams.io/posts/git-commit/
[OneFlow]: http://endoflineblog.com/oneflow-a-git-branching-model-and-workflow
[GNU GPLv3 license]: ./LICENSE
